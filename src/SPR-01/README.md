This repository contains the firmware for a node with:

* Sonoff
* Sonoff IO Shield
* Reed sensor (connected to GPIO5 and GND)
* PIR sensor (connected to GPIO12)

The internal objects implement Active.W and use IBool.W iface, from IDM.

WARNING: this repository is linked on documentation. If you move it,
please update the following doc:

* IoT Integration (on gitbook)
